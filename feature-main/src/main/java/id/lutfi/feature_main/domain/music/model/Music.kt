
package id.lutfi.feature_main.domain.music.model

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version Music, v 0.1 21/03/2024 04:43 by Lutfi Rizky Ramadhan
 */
class Music(
    val trackName: String = "",
    val artistName: String = "",
    val collectionName: String = "",
    val previewUrl: String = "",
    val artworkUrl: String = "",
)