
package id.lutfi.feature_main.domain.music.interactor

import id.lutfi.core.base.BaseUseCase
import id.lutfi.feature_main.domain.music.MusicRepository
import id.lutfi.feature_main.domain.music.model.Music
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version SearchMusic, v 0.1 21/03/2024 03:44 by Lutfi Rizky Ramadhan
 */
class SearchMusic @Inject constructor(
    private val repository: MusicRepository
): BaseUseCase<SearchMusic.Params, List<Music>>() {

    override fun build(param: Params): Flowable<List<Music>> {
        return repository.searchMusic(param.query)
    }

    class Params(val query: String)
}