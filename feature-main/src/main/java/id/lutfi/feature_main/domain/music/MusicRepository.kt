
package id.lutfi.feature_main.domain.music

import id.lutfi.feature_main.domain.music.model.Music
import io.reactivex.rxjava3.core.Flowable

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicRepository, v 0.1 21/03/2024 03:42 by Lutfi Rizky Ramadhan
 */
interface MusicRepository {

    fun searchMusic(query: String): Flowable<List<Music>>
}