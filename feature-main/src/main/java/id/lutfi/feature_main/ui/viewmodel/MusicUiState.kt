package id.lutfi.feature_main.ui.viewmodel

import id.lutfi.feature_main.ui.model.MusicModel

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicUiState, v 0.1 21/03/2024 05:02 by Lutfi Rizky Ramadhan
 */
sealed interface MusicUiState {

    object None : MusicUiState
    class onSuccessSearchMusic(val musicList: List<MusicModel>) : MusicUiState
    class onErrorSearchMusic(val throwable: Throwable) : MusicUiState
}