package id.lutfi.feature_main.ui.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import id.lutfi.feature_main.data.music.MusicRepositoryImpl
import id.lutfi.feature_main.data.music.source.MusicDataFactory
import id.lutfi.feature_main.domain.music.MusicRepository

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version RepositoryModule, v 0.1 21/03/2024 05:51 by Lutfi Rizky Ramadhan
 */
@InstallIn(ViewModelComponent::class)
@Module
class RepositoryModule {

    @Provides
    @ViewModelScoped
    fun musicRepository(musicDataFactory: MusicDataFactory): MusicRepository =
        MusicRepositoryImpl(musicDataFactory)
}