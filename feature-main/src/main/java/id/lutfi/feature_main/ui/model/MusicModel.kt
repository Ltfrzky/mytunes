package id.lutfi.feature_main.ui.model

import id.lutfi.feature_main.domain.music.model.Music

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicModel, v 0.1 21/03/2024 04:47 by Lutfi Rizky Ramadhan
 */
class MusicModel(
    val trackName: String = "",
    val artistName: String = "",
    val collectionName: String = "",
    val previewUrl: String = "",
    val artworkUrl: String = "",
    var state: State = State.DEFAULT
)

enum class State {
    DEFAULT, PLAYING
}

fun Music.toMusicModel() = MusicModel(
    trackName, artistName, collectionName, previewUrl, artworkUrl
)

fun List<Music>.toMusicModels() = map { it.toMusicModel() }