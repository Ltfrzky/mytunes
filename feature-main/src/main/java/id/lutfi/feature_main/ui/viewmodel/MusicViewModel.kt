package id.lutfi.feature_main.ui.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.lutfi.feature_main.domain.music.interactor.SearchMusic
import id.lutfi.feature_main.ui.model.toMusicModels
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicViewModel, v 0.1 21/03/2024 05:01 by Lutfi Rizky Ramadhan
 */
@HiltViewModel
class MusicViewModel @Inject constructor(
    private val searchMusic: SearchMusic,
) : ViewModel() {

    private val _uiState = MutableStateFlow<MusicUiState>(MusicUiState.None)
    val uiState = _uiState.asStateFlow()

    fun searchMusic(query: String) {
        searchMusic.execute(
            SearchMusic.Params(query),
            onSuccess = { musicList ->
                _uiState.update { MusicUiState.onSuccessSearchMusic(musicList.toMusicModels()) }
            }, onError = { error ->
                _uiState.update { MusicUiState.onErrorSearchMusic(error) }
            })
    }
}