package id.lutfi.feature_main.ui.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.lutfi.feature_main.data.music.source.MusicDataFactory
import id.lutfi.feature_main.data.music.source.local.LocalMusicData
import id.lutfi.feature_main.data.music.source.network.MusicApi
import id.lutfi.feature_main.data.music.source.network.NetworkMusicData
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version ApiModule, v 0.1 21/03/2024 05:49 by Lutfi Rizky Ramadhan
 */
@InstallIn(SingletonComponent::class)
@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideMusicApi(retrofit: Retrofit): MusicApi = retrofit.create(MusicApi::class.java)

    @Provides
    @Singleton
    fun provideMusicDataFactory(
        localMusicData: LocalMusicData,
        networkMusicData: NetworkMusicData,
    ) = MusicDataFactory(localMusicData, networkMusicData)
}