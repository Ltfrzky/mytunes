package id.lutfi.feature_main.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import id.lutfi.core.base.BaseRecyclerViewAdapter
import id.lutfi.core.base.BaseViewHolder
import id.lutfi.feature_main.R
import id.lutfi.feature_main.databinding.ItemSongBinding
import id.lutfi.feature_main.ui.model.MusicModel
import id.lutfi.feature_main.ui.model.State

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicAdapter, v 0.1 21/03/2024 06:01 by Lutfi Rizky Ramadhan
 */
class MusicAdapter(
    private val data: MutableList<MusicModel> = mutableListOf(),
    private val onClickItem: (MusicModel) -> Unit = {},
) : BaseRecyclerViewAdapter<MusicModel>(data) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<MusicModel> {
        return MusicViewHolder(
            ItemSongBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder<MusicModel>, position: Int) {
        holder.bind(data[position])
    }

    fun selectTrack(musicModel: MusicModel) {
        deselectTrack()
        val position = data.indexOf(musicModel)
        val newData = data.find { it.previewUrl == musicModel.previewUrl }?.let {
            it.state = State.PLAYING
            it
        }
        notifyItemChanged(position, newData)
    }

    fun deselectTrack() {
        data.find { it.state == State.PLAYING}?.let {
            it.state = State.DEFAULT
            it
        }?.let {
            val position = data.indexOf(it)
            notifyItemChanged(position, it)
        }
    }

    inner class MusicViewHolder(
        private val binding: ItemSongBinding,
    ) : BaseViewHolder<MusicModel>(binding.root) {

        override fun bind(data: MusicModel) {
            binding.run {
                tvSongName.text = data.trackName
                tvSongArtist.text = data.artistName
                tvSongAlbum.text = data.collectionName
                imgSong.loadImage(data.artworkUrl)
                root.setOnClickListener {
                    onClickItem.invoke(data)
                }

                imgSelected.isVisible = when (data.state) {
                    State.PLAYING -> true
                    else -> false
                }
            }

        }

        private fun ImageView.loadImage(url: String) {
            Glide.with(context)
                .load(url)
                .placeholder(R.drawable.ic_pause)
                .into(this)
        }
    }
}