package id.lutfi.feature_main.ui

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.lutfi.core.base.BaseActivity
import id.lutfi.feature_main.R
import id.lutfi.feature_main.databinding.ActivityMainBinding
import id.lutfi.feature_main.ui.adapter.MusicAdapter
import id.lutfi.feature_main.ui.model.MusicModel
import id.lutfi.feature_main.ui.viewmodel.MusicUiState
import id.lutfi.feature_main.ui.viewmodel.MusicViewModel
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val musicViewModel: MusicViewModel by viewModels()
    private val musicAdapter: MusicAdapter = MusicAdapter() {
        showMusicPlayer(it)
    }
    private var currentlySelectedTrack: MusicModel? = null

    override fun init(savedInstanceState: Bundle?) {
        initRecyclerView()
        initViewModel()
        initAction()
    }

    private fun initViewModel() {
        musicViewModel.uiState.flowWithLifecycle(lifecycle)
            .distinctUntilChanged()
            .onEach {
                when (it) {
                    is MusicUiState.onErrorSearchMusic -> {
                        renderEmptyState()
                        resetUI()
                    }

                    is MusicUiState.onSuccessSearchMusic -> {
                        if (it.musicList.isNotEmpty()) {
                            binding.tvStatus.isVisible = false
                            renderLoading(false)
                            binding.rvMusic.isVisible = true
                            musicAdapter.removeAllItem()
                            musicAdapter.appendItems(it.musicList)
                        } else {
                            renderEmptyState()
                        }
                    }

                    else -> {
                        //do nothing
                    }
                }
            }.launchIn(lifecycleScope)
    }

    private fun initRecyclerView() {
        binding.rvMusic.run {
            layoutManager =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
            adapter = musicAdapter
            addItemDecoration(DividerItemDecoration(this@MainActivity, LinearLayoutManager.VERTICAL).apply {
                ContextCompat.getDrawable(this@MainActivity, R.drawable.divider_item)?.let { setDrawable(it) }
            })
        }
    }

    private fun initAction() {
        binding.tilSearchArtist.editText?.setOnEditorActionListener { textView, i, _ ->
            if (i == EditorInfo.IME_ACTION_DONE) {
                searchMusic(textView.text.toString())
            }
            return@setOnEditorActionListener false
        }
    }

    private fun searchMusic(query: String) {
        if (query.isBlank()) return

        resetUI()
        renderLoading(true)
        musicViewModel.searchMusic(query)
    }

    private fun renderLoading(isVisible: Boolean) {
        binding.loading.isVisible = isVisible
    }

    private fun showMusicPlayer(musicModel: MusicModel) {
        if (currentlySelectedTrack?.previewUrl != musicModel.previewUrl) {
            musicAdapter.selectTrack(musicModel)
            binding.viewMusicControl.run {
                isVisible = true
                openMusicControl(musicModel.previewUrl)
            }
        }
    }

    private fun renderEmptyState() {
        renderLoading(false)
        musicAdapter.removeAllItem()
        binding.rvMusic.recycledViewPool.clear()
        musicAdapter.notifyDataSetChanged()
        binding.tvStatus.run {
            isVisible = true
            text = getString(R.string.error)
        }
    }

    override fun onStop() {
        super.onStop()
        resetUI()
    }

    private fun resetUI() {
        binding.viewMusicControl.stopMusic()
        binding.viewMusicControl.isVisible = false
        musicAdapter.deselectTrack()
    }
}