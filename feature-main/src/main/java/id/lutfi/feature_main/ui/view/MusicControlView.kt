package id.lutfi.feature_main.ui.view

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.SeekBar
import androidx.lifecycle.findViewTreeLifecycleOwner
import androidx.lifecycle.lifecycleScope
import id.lutfi.core.base.BaseRichView
import id.lutfi.feature_main.R
import id.lutfi.feature_main.databinding.ViewMusicControlBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicControlView, v 0.1 21/03/2024 02:47 by Lutfi Rizky Ramadhan
 */
class MusicControlView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0,
) : BaseRichView<ViewMusicControlBinding>(context, attrs, defStyleAttr, defStyleRes) {

    private var isPlaying: Boolean = false
        set(value) {
            field = value
            if (value) onMusicPlayClick()
            else onMusicPauseClick()
        }
    private var mediaPlayer: MediaPlayer? = null
    private var currentlyPlaying: String = ""

    override fun inflateViewBinding(): ViewMusicControlBinding = ViewMusicControlBinding.inflate(
        LayoutInflater.from(context)
    )

    override fun setup() {
        setupOnClickCallback()
        setupSeekBar()
    }

    private fun setupOnClickCallback() {
        binding.run {
            btnPlayPause.setOnClickListener {
                isPlaying = !isPlaying
            }
        }
    }

    private fun setupSeekBar() {
        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, p2: Boolean) {
                //do nothing
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
                this@MusicControlView.isPlaying = false
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                mediaPlayer?.seekTo(seekBar.progress)
                this@MusicControlView.isPlaying = true
            }

        })
    }

    fun openMusicControl(soundUrl: String) {
        if (currentlyPlaying == soundUrl) return
        stopMusic()
        mediaPlayer = MediaPlayer.create(context, Uri.parse(soundUrl))
        mediaPlayer?.setOnCompletionListener {
            isPlaying = false
        }
        mediaPlayer?.start()
        currentlyPlaying = soundUrl
        isPlaying = true
        attachSeekBar()
    }

    fun stopMusic() {
        if (mediaPlayer != null) {
            mediaPlayer?.stop()
            mediaPlayer?.release()
            mediaPlayer = null
            isPlaying = false
            currentlyPlaying = ""
            detachSeekBar()
        }
    }

    private fun onMusicPlayClick() {
        mediaPlayer?.let {
            if (it.duration == it.currentPosition) {
                it.seekTo(0)
                binding.seekBar.progress = 0
                attachSeekBar()
            }
            binding.btnPlayPause.setImageResource(R.drawable.ic_pause)
            mediaPlayer?.start()
        }
    }

    private fun onMusicPauseClick() {
        binding.btnPlayPause.setImageResource(R.drawable.ic_play)
        mediaPlayer?.pause()
    }

    private fun attachSeekBar() {
        mediaPlayer?.let {
            binding.seekBar.run {
                progress = 0
                max = it.duration
            }
            startSeekBar()
        }
    }

    private fun startSeekBar() {
        mediaPlayer?.let {
            findViewTreeLifecycleOwner()?.lifecycleScope?.launch {
                var currentPosition = it.currentPosition
                val total = it.duration
                while (mediaPlayer != null && it.isPlaying && currentPosition < total) {
                    currentPosition = try {
                        delay(1_000)
                        it.currentPosition
                    } catch (e: Exception) {
                        return@launch
                    }
                    binding.seekBar.progress = currentPosition
                }
            }
        }
    }

    private fun detachSeekBar() {
        binding.seekBar.progress = 0
    }


}