
package id.lutfi.feature_main.data.music.source.network

import id.lutfi.feature_main.data.music.source.model.SearchMusicResponse
import io.reactivex.rxjava3.core.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicApi, v 0.1 21/03/2024 03:50 by Lutfi Rizky Ramadhan
 */
interface MusicApi {

    @GET("/search")
    fun searchMusic(@Query("term") query: String): Flowable<SearchMusicResponse>
}