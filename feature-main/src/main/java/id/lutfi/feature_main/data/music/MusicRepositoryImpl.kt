package id.lutfi.feature_main.data.music

import id.lutfi.core.base.Source
import id.lutfi.feature_main.data.music.source.MusicDataFactory
import id.lutfi.feature_main.data.music.source.model.toMusicList
import id.lutfi.feature_main.domain.music.MusicRepository
import id.lutfi.feature_main.domain.music.model.Music
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicRepositoryImpl, v 0.1 21/03/2024 03:49 by Lutfi Rizky Ramadhan
 */
class MusicRepositoryImpl @Inject constructor(
    private val musicDataFactory: MusicDataFactory,
) : MusicRepository {

    private val networkMusicData by lazy {
        musicDataFactory.create(Source.NETWORK)
    }

    override fun searchMusic(query: String): Flowable<List<Music>> {
        return networkMusicData.searchMusic(query)
            .map {
                it.results?.toMusicList().orEmpty()
            }
    }
}