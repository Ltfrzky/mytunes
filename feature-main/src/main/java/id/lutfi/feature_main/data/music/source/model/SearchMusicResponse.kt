
package id.lutfi.feature_main.data.music.source.model

import id.lutfi.feature_main.domain.music.model.Music

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version SearchMusicResponse, v 0.1 21/03/2024 04:40 by Lutfi Rizky Ramadhan
 */
class SearchMusicResponse(
    val results: List<MusicResponse>? = emptyList(),
    var term: String? = ""
) {
    fun hasValidResult(): Boolean {
        return !results.isNullOrEmpty()
    }
}

fun MusicResponse.toMusic() = Music(
    trackName = trackName.orEmpty(),
    artistName = artistName.orEmpty(),
    collectionName = collectionName.orEmpty(),
    previewUrl = previewUrl.orEmpty(),
    artworkUrl = artworkUrl.orEmpty()
)

fun List<MusicResponse>.toMusicList() = map { it.toMusic() }