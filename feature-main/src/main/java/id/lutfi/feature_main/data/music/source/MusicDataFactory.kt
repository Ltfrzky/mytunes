
package id.lutfi.feature_main.data.music.source

import id.lutfi.core.base.Source
import id.lutfi.feature_main.data.music.MusicData
import javax.inject.Inject

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicDataFactory, v 0.1 21/03/2024 04:48 by Lutfi Rizky Ramadhan
 */
class MusicDataFactory @Inject constructor(
    private val localData: MusicData,
    private val networkData: MusicData,
) {

    fun create(source: Source): MusicData {
        return when (source) {
            Source.LOCAL -> localData
            else -> networkData
        }
    }
}