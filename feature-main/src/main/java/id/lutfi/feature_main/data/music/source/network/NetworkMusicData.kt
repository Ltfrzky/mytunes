package id.lutfi.feature_main.data.music.source.network

import id.lutfi.feature_main.data.music.MusicData
import id.lutfi.feature_main.data.music.source.model.SearchMusicResponse
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version NetworkMusicData, v 0.1 21/03/2024 03:50 by Lutfi Rizky Ramadhan
 */
class NetworkMusicData @Inject constructor(
    private val musicApi: MusicApi,
) : MusicData {

    override fun searchMusic(query: String): Flowable<SearchMusicResponse> {
        return musicApi.searchMusic(query)
    }
}