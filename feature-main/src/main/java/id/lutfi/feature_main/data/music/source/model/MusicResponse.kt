
package id.lutfi.feature_main.data.music.source.model

import com.google.gson.annotations.SerializedName

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicResponse, v 0.1 21/03/2024 04:39 by Lutfi Rizky Ramadhan
 */
class MusicResponse(
    val trackName: String? = "",
    val artistName: String? = "",
    val collectionName: String? = "",
    val previewUrl: String? = "",
    @SerializedName("artworkUrl60")
    val artworkUrl: String? = "",
)