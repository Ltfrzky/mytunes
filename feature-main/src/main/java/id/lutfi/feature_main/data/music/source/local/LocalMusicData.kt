package id.lutfi.feature_main.data.music.source.local

import androidx.datastore.preferences.core.stringPreferencesKey
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.lutfi.core.storage.DataStoreManager
import id.lutfi.feature_main.data.music.MusicData
import id.lutfi.feature_main.data.music.source.model.SearchMusicResponse
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version LocalMusicData, v 0.1 21/03/2024 04:50 by Lutfi Rizky Ramadhan
 */
@OptIn(ExperimentalCoroutinesApi::class)
class LocalMusicData @Inject constructor(
    private val dataStoreManager: DataStoreManager,
    private val gson: Gson,
) : MusicData {

    override fun searchMusic(query: String): Flowable<SearchMusicResponse> {
        val searchTerm = stringPreferencesKey("$SEARCH_TERM$query")
        return dataStoreManager.rxAppDataStore.data().map {
            it[searchTerm]?.let { savedResponse ->
                gson.fromJson(
                    savedResponse,
                    object : TypeToken<SearchMusicResponse>() {}.type
                )
            } ?: SearchMusicResponse()
        }
    }

    override fun saveTermResponse(searchMusicResponse: SearchMusicResponse) {
        val searchTerm = stringPreferencesKey("$SEARCH_TERM${searchMusicResponse.term}")
        dataStoreManager.rxAppDataStore.updateDataAsync {
            val mutablePref = it.toMutablePreferences()
            mutablePref[searchTerm] = gson.toJson(searchMusicResponse.results)
            Single.just(mutablePref)
        }
    }

    companion object {

        private const val SEARCH_TERM = "SEARCH_TERM_"
    }
}