
package id.lutfi.feature_main.data.music

import id.lutfi.feature_main.data.music.source.model.SearchMusicResponse
import io.reactivex.rxjava3.core.Flowable

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicData, v 0.1 21/03/2024 03:42 by Lutfi Rizky Ramadhan
 */
interface MusicData {

    fun searchMusic(query: String): Flowable<SearchMusicResponse>

    fun saveTermResponse(searchMusicResponse: SearchMusicResponse) {
        throw NotImplementedError()
    }
}