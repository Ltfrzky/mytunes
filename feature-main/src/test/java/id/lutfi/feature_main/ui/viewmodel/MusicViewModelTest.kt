package id.lutfi.feature_main.ui.viewmodel

import assertk.assertThat
import assertk.assertions.isInstanceOf
import id.lutfi.core.base.OnErrorCallback
import id.lutfi.core.base.OnSuccessCallback
import id.lutfi.feature_main.domain.music.interactor.SearchMusic
import id.lutfi.feature_main.domain.music.model.Music
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicViewModelTest, v 0.1 21/03/2024 11:58 by Lutfi Rizky Ramadhan
 */
class MusicViewModelTest {

    private val searchMusic: SearchMusic = mockk(relaxed = true)
    private val viewModel = MusicViewModel(searchMusic)

    private fun stubSearchMusicSuccess() {
        every { searchMusic.execute(any(), any(), any(), any()) } answers {
            this.arg<OnSuccessCallback<List<Music>>>(1).invoke(listOf())
        }
    }

    private fun stubSearchMusicError() {
        every { searchMusic.execute(any(), any(), any(), any()) } answers {
            this.arg<OnErrorCallback>(2).invoke(Throwable())
        }
    }

    @Test
    fun `searchMusicUseCase success should update uiState to MusicUiState#onSuccessSearchMusic`() {
        stubSearchMusicSuccess()

        viewModel.searchMusic("asd")

        assertThat(viewModel.uiState.value).isInstanceOf(MusicUiState.onSuccessSearchMusic::class.java)
        verify { searchMusic.execute(any(), any(), any(), any()) }
    }

    @Test
    fun `searchMusicUseCase success should update uiState to MusicUiState#onErrorSearchMusic`() {
        stubSearchMusicError()

        viewModel.searchMusic("asd")

        assertThat(viewModel.uiState.value).isInstanceOf(MusicUiState.onErrorSearchMusic::class.java)
        verify { searchMusic.execute(any(), any(), any(), any()) }
    }
}