package id.lutfi.feature_main.data.music

import assertk.assertThat
import assertk.assertions.isEmpty
import assertk.assertions.isEqualTo
import id.lutfi.core.base.Source
import id.lutfi.feature_main.data.music.source.MusicDataFactory
import id.lutfi.feature_main.data.music.source.model.MusicResponse
import id.lutfi.feature_main.data.music.source.model.SearchMusicResponse
import id.lutfi.feature_main.data.music.source.network.NetworkMusicData
import id.lutfi.feature_main.domain.music.MusicRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.rxjava3.core.Flowable
import org.junit.Before
import org.junit.Test

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version MusicRepositoryImplTest, v 0.1 21/03/2024 11:44 by Lutfi Rizky Ramadhan
 */
class MusicRepositoryImplTest {

    private val dataFactory: MusicDataFactory = mockk(relaxed = true)
    private val networkData: NetworkMusicData = mockk(relaxed = true)
    private val repository: MusicRepository = MusicRepositoryImpl(dataFactory)

    @Before
    fun setup() {
        every { dataFactory.create(Source.NETWORK) } returns networkData
    }

    private fun stubMusicResponse() = MusicResponse("a", "a", "a", "a", "a")

    private fun stubSearchMusicResponse() = SearchMusicResponse(listOf(stubMusicResponse()))

    @Test
    fun `searchMusic should return correct value`() {
        //GIVEN
        val response = stubSearchMusicResponse()
        every { networkData.searchMusic(any()) } returns Flowable.just(response)

        //WHEN
        val result = repository.searchMusic("asd").blockingFirst()

        //THEN
        assertThat(result.first().artistName).isEqualTo(response.results!!.first().artistName)
        assertThat(result.first().artworkUrl).isEqualTo(response.results!!.first().artworkUrl)
        assertThat(result.first().previewUrl).isEqualTo(response.results!!.first().previewUrl)
        assertThat(result.first().collectionName).isEqualTo(response.results!!.first().collectionName)
        assertThat(result.first().trackName).isEqualTo(response.results!!.first().trackName)
        verify { networkData.searchMusic(any()) }
    }

    @Test
    fun `searchMusic with null result should return correct value`() {
        //GIVEN
        val response = SearchMusicResponse(null)
        every { networkData.searchMusic(any()) } returns Flowable.just(response)

        //WHEN
        val result = repository.searchMusic("asd").blockingFirst()

        //THEN
        assertThat(result).isEmpty()
        verify { networkData.searchMusic(any()) }
    }
}