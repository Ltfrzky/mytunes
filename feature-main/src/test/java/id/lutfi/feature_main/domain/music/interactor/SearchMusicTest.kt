package id.lutfi.feature_main.domain.music.interactor

import id.lutfi.feature_main.domain.music.MusicRepository
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version SearchMusicTest, v 0.1 21/03/2024 11:56 by Lutfi Rizky Ramadhan
 */
class SearchMusicTest {
    private val repository: MusicRepository = mockk(relaxed = true)
    private val useCase = SearchMusic(repository)

    @Test
    fun `build should call repository#searchMusic`() {
        useCase.build(SearchMusic.Params("asd"))

        verify { repository.searchMusic(any()) }
    }
}