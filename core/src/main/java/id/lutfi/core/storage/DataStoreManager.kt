package id.lutfi.core.storage

import android.content.Context
import androidx.datastore.preferences.rxjava3.rxPreferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version DataStoreManager, v 0.1 21/03/2024 01:56 by Lutfi Rizky Ramadhan
 */
class DataStoreManager @Inject constructor(@ApplicationContext context: Context) {

    val rxAppDataStore = context.rxDataStore
}

private val Context.rxDataStore by rxPreferencesDataStore("mytunes")