package id.lutfi.core.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version BaseViewHolder, v 0.1 21/03/2024 01:53 by Lutfi Rizky Ramadhan
 */
abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun bind(data: T)

}