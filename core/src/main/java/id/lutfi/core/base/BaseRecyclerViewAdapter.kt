package id.lutfi.core.base

import androidx.recyclerview.widget.RecyclerView

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version BaseRecyclerViewAdapter, v 0.1 21/03/2024 01:55 by Lutfi Rizky Ramadhan
 */
abstract class BaseRecyclerViewAdapter<T>(
    private var data: MutableList<T>
) : RecyclerView.Adapter<BaseViewHolder<T>>() {

    override fun getItemCount(): Int = getDataSize()

    private fun getDataSize() = if (data == null) 0 else data.size

    fun removeAllItem() {
        if (data == null || data.isEmpty()) return

        data.clear()
        notifyItemRangeRemoved(0, getDataSize())
    }

    fun appendItems(items: List<T>) {
        if (items.isEmpty()) return

        data.addAll(items)
        notifyDataSetChanged()
    }
}