
package id.lutfi.core.base

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.viewbinding.ViewBinding

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version BaseRichView, v 0.1 21/03/2024 02:49 by Lutfi Rizky Ramadhan
 */
abstract class BaseRichView<VB: ViewBinding>: FrameLayout {
    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?): super(context, attrs) {
        init(context, attrs)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ): super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ): super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs)
    }

    abstract fun inflateViewBinding(): VB

    protected val binding: VB by lazy(LazyThreadSafetyMode.NONE) { inflateViewBinding() }

    protected fun init(context: Context, attrs: AttributeSet?) {
        addView(binding.root)
        initEvent(context, attrs)
    }

    private fun initEvent(context: Context, attrs: AttributeSet?) {
        parseAttrs(context, attrs)
        setup()
    }

    protected open fun parseAttrs(context: Context?, attrs: AttributeSet?) {
        // no-op
    }

    open fun setup() {
        // default implementation
    }

}