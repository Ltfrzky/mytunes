package id.lutfi.core.base

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version Source, v 0.1 21/03/2024 01:52 by Lutfi Rizky Ramadhan
 */
enum class Source {
    LOCAL,
    NETWORK
}