package id.lutfi.core.base

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import timber.log.Timber

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version BaseUseCase, v 0.1 21/03/2024 01:54 by Lutfi Rizky Ramadhan
 */
abstract class BaseUseCase<Param, Result : Any>(
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    private val disposable = CompositeDisposable()

    abstract fun build(param: Param): Flowable<Result>

    fun execute(
        param: Param,
        onSuccess: (Result) -> Unit = {},
        onError: (Throwable) -> Unit = {},
        onCompletion: () -> Unit = {},
    ) {
        build(param)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                onSuccess.invoke(it)
            }, {
                Timber.e(it)
                onError.invoke(it)
                dispose()
            }, {
                onCompletion.invoke()
                dispose()
            }).run { disposable.add(this) }
    }

    private fun dispose() {
        disposable.clear()
    }
}

typealias OnSuccessCallback<T> = (T) -> Unit
typealias OnErrorCallback = (Throwable) -> Unit
typealias OnCompleteCallback = () -> Unit