package id.lutfi.core.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import id.lutfi.core.utils.getBinding

/**
 * @author Lutfi Rizky Ramadhan (lutfirizkyramadhan@gmail.com)
 * @version BaseActivity, v 0.1 21/03/2024 01:59 by Lutfi Rizky Ramadhan
 */
abstract class BaseActivity<V : ViewBinding> : AppCompatActivity() {

    lateinit var binding: V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getBinding()
        setContentView(binding.root)

        init(savedInstanceState)
    }

    protected abstract fun init(savedInstanceState: Bundle?)
}