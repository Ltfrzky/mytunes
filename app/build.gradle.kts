import java.io.FileInputStream
import java.util.Properties

@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    alias(libs.plugins.com.android.application)
    alias(libs.plugins.org.jetbrains.kotlin.android)
    id("dagger.hilt.android.plugin")
    kotlin("kapt")
}

val baseEnvFile = rootProject.file("base-env.properties")
val baseEnvProp = Properties()
baseEnvProp.load(FileInputStream(baseEnvFile))

android {
    namespace = "id.lutfi.mytunes"
    compileSdk = 34

    defaultConfig {
        applicationId = "id.lutfi.mytunes"
        minSdk = 24
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        debug {
            multiDexEnabled = true
            isDebuggable = true
            buildConfigField("String", "BASE_URL", "${baseEnvProp["BASE_URL"]}")
        }
        release {
            multiDexEnabled = true
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            buildConfigField("String", "BASE_URL", "${baseEnvProp["BASE_URL"]}")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }

    buildFeatures {
        buildConfig = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
    hilt {
        enableAggregatingTask = true
    }
}

dependencies {
    implementation(project(":core"))
    implementation(project(":feature-main"))
    api(libs.bundles.feature.dep.api)
    kapt(libs.bundles.feature.dep.kapt)
    implementation(libs.multidex)
    testImplementation(libs.bundles.core.dep.test)
}

kapt {
    correctErrorTypes = true
}